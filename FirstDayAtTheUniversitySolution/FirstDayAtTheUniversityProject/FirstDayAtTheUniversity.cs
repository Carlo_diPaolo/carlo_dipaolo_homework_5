using System;

class FirstDayAtUni
{
/// <summary>
/// The entry point of the program, where the program control starts and ends.
/// </summary>
	public static void Main()
	{
		PrintIntroText ();
		Choice01 ();
		PrintOutroText ();

		}

/// <summary>
/// The introduction to the story where the student is arriving late to class and his choice to hurry or not.
/// </summary>
	static void PrintIntroText ()
	{
		Console.WriteLine("It's your first day at the university.\n"+
						  "Today you will beginn to learn about code and, hopefully all those weird "+
						  "lines of text you have seen in movies will start to make sense.\n" +
						  "As you daydream about Hackers and Fireawalls (and dodging bullets in slow-mo for some reason)," +
						  " you realise you are running terribly late.\n\n" +
						  "[Press 1 to pick up the pace and hurry to the Uni, press 9 to keep walking like a cool dude.]\n");
	}
/// <summary>
/// Prints the text for choice 02, after the player has hurried to class.
/// </summary>
	static void PrintChoice_02_Text ()
	{
		Console.Clear();
		Console.WriteLine("You tell yourself that you're never going to understand anythig about programming if you miss the basics, so you run to the University.\n"+
						  "You make it to class just in time and jump through the closing door a mere moment before the evil magnetic lock can snap close," + 
						  " banishing you forever to the hallways.\n\n" +
						  "[Press 1 to pick one of the seats that are still free, press 9 to show how excited you are.]\n");

	}
/// <summary>
/// Prints the text for choice 03, after the player has decidet to take his time.
/// </summary>
	static void PrintChoice_03_Text ()
	{
		Console.Clear();
		Console.WriteLine("You tell yourself that if you want to be a real hacker and fight the system, you can't let people tell you where to be and at what time.\n" +
						  "So you keep strolling down the road without a worry in the world, whislting and stopping every now and then to smell a flower or pet a squirrel.\n\n" + 
						  "[Press 1 to maybe hurry just a little in the general direction of the school, press 9 to deepen you connection with Nature.]\n");
	}
/// <summary>
/// A wrong key has been pressed and the game universe implodes.
/// </summary>
	static void WrongKey ()
	{
		Console.Clear ();
		Console.WriteLine("You pressed an invalid Key!\n\n"+
						  "You fall to your knees and scream as reality folds on itself hundreds of times before exploding into nothingness.\n\n" +
						  "You feel a little hungry.");

	}
/// <summary>
/// The Player's first choice...to hurry or not to hurry?
/// </summary>
	static void Choice01 ()
	{
		char firstKey = Console.ReadKey().KeyChar;
		Console.WriteLine ();



		if(firstKey == '1')
		{
			Choice02_AfterHurry ();
		}

		else if(firstKey == '9')
		{
			Choice03_AfterChill ();

		}

		else
		{
			Console.Clear ();
			PrintIntroText ();
			Choice01();
		}

	}
/// <summary>
/// The second choice. Be a normal student or go crazy with excitement?
/// </summary>
	static void Choice02_AfterHurry ()
	{
		PrintChoice_02_Text ();

		char secondkey = Console.ReadKey().KeyChar;

		if(secondkey == '1')
		{
			Console.Clear();
			Console.WriteLine("You take place at one of the computers and push the power button.\n\n" +
							  "It turns on.\n\n" +
							  "So far so good.");
		}

		else if(secondkey =='9')
		{
			Console.Clear();
			Console.WriteLine("While everyone else takes their places you stanrt flexing and screaming:\n" +
							  "YEEEEAH LET'S DO THIS!!! LET'S HACK INTO SOME FIREWALLS!!!\n" +
							  "Everyone is looking at you with pity or concern. The Professor comes close and asks you if you are feeling alright.");

		
		}
		else
		{
			Console.Clear ();
			Choice02_AfterHurry ();
		
		}

	}
/// <summary>
/// Third choice: Try to go to class or just give up?
/// </summary>
	static void Choice03_AfterChill ()
	{
		PrintChoice_03_Text ();

		char secondkey = Console.ReadKey().KeyChar;
		Console.WriteLine();

		if(secondkey == '1')
		{
			Console.Clear();
			Console.WriteLine("you arrive at the Uni just as everybody is going home, so you decide to stay in the class.\n" +
							  "You are such a rebel\n.");

		}

		else if(secondkey == '9')
		{
			Console.Clear();
			Console.WriteLine("As you listen to the animals and stroke the leaves, you feel like you are starting to understand what they say to each other.\n" +
							  "You are becoming one with the Froest.\n" +
							  "Or maybe those berries you ate are giving you allucinations.\n");

		}

		else
		{
			Console.Clear();
			Choice03_AfterChill ();


		}

	}

	static void PrintOutroText ()
	{
		
		Console.WriteLine("\n\nThanks for Playing!");
	}
}
